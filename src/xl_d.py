#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" openpyxlを使用し、月単位の予定表Excelシートを作成するサンプルプログラム。 """

__author__    = "@HirMtsd"
__copyright__ = "Copyright 2022, Matsuda.Hironobu"
__date__      = "2022/09/07"
__email__     = "hirmtsd@gmail.com"
__version__   = "0.1"
__status__    = "Development"

# openpyxlについては、以下を参照
# https://openpyxl.readthedocs.io/
# venvで仮想環境を作成し、以下のコマンドでインストールすることを推奨。
# pip install openpyxl

# Windows環境で、venvを使用している場合、shebangのせいで、
# 「ModuleNotFoundError: No module named 'openpyxl'」
# が表示される場合は、pyランチャを使用しないで「python xl_d.py」と起動する。
# https://www.python.org/dev/peps/pep-0397/

from openpyxl import Workbook
from openpyxl import utils
from openpyxl.formula.translate import Translator
from openpyxl.formatting import Rule
from openpyxl.styles.differential import DifferentialStyle
from openpyxl.styles import PatternFill
import datetime

# 新規ワークブックを作成
wb = Workbook()

# シートを1つ準備し、名前を付ける
ws = wb.active
ws.title = 'Month'

# 日付の基点を設定
ds = datetime.date(2022,1, 1)

# 基点のセルを設定 (C2=2,3)
ws['C2'] = ds
ws['C2'].number_format = 'dd'

# 基点の横のセルを設定 (D2=2,4)
ws['D2'] = '=C2+1'
ws['D2'].number_format = 'dd'

# ループ処理(日付を展開)
for i in range(3, 32):
  # セル番号(数値から英字に変換)
  cell_no = utils.get_column_letter(i + 2) + '2'
  # セルの計算式を基点のセルからコピー
  ws.cell(2, i + 2).value = Translator(ws['D2'].value, origin='D2').translate_formula(cell_no)
  # 書式を設定
  ws.cell(2, i + 2).number_format = 'dd'

# 曜日を設定 (C3=3,3)
ws['C3'] = '=TEXT(C2,"aaa")'

# ループ処理(曜日を展開)
for i in range(2, 32):
  # セル番号(数値から英字に変換)
  cell_no = utils.get_column_letter(i + 2) + '3'
  # セルの計算式を基点のセルからコピー
  ws.cell(3, i + 2).value = Translator(ws['C3'].value, origin='C3').translate_formula(cell_no)

# 条件付き書式の設定
# 「数式を使用して、書式設定するセルを決定」
blue_fill = PatternFill(patternType='solid', bgColor='0000F0')
blue_ds = DifferentialStyle(fill=blue_fill)
sat_rl = Rule(type='expression', dxf = blue_ds)
sat_rl.formula = ['=EXACT(C$3,"土")']
red_fill = PatternFill(patternType='solid', bgColor='F00000')
red_ds = DifferentialStyle(fill=red_fill)
sun_rl = Rule(type='expression', dxf = red_ds)
sun_rl.formula = ['=EXACT(C$3,"日")']
ws.conditional_formatting.add('$C$2:$AG$51', sat_rl)
ws.conditional_formatting.add('$C$2:$AG$51', sun_rl)

# ファイルを保存
wb.save("month_sample.xlsx")
